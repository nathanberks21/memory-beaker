const { ipcMain } = require('electron');

const processHandler = require('./processHandler');

ipcMain.on('get-all-processes', (event) => {
    processHandler.getAll().then(allProcesses =>
        event.sender.send('get-all-processes-reply', allProcesses));
});
