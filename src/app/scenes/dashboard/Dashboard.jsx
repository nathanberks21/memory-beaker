import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';

import './Dashboard.css';

import { processes$ } from '../../api/processes';

import ProcessTable from '../../components/processTable/ProcessTable.jsx';
import Viewer from '../../components/viewer/Viewer.jsx';

export default class Dashboard extends React.Component {
    constructor() {
        super();
        this.state = {
            processList: {},
            secondsPassed: 0,
            selectedPids: new Set()
        };
        processes$.subscribe(({ processList, secondsPassed }) => {
            this.setState({ processList, secondsPassed });
        });
    }

    render() {
        return (
            <div>
                <Viewer selectedPids={this.state.selectedPids}/>
                <ProcessTable
                    processList={this.state.processList}
                    selectProcess={this.selectProcess.bind(this)}
                    secondsPassed={this.state.secondsPassed}
                    selectedPids={this.state.selectedPids}/>
            </div>
        );
    }

    selectProcess(pid) {
        const updatedProcesses =  new Set(this.state.selectedPids);
        if (updatedProcesses.has(pid)) {
            updatedProcesses.delete(pid);
        } else {
            updatedProcesses.add(pid);
        }
        this.setState({ selectedPids: updatedProcesses });
    }
}
