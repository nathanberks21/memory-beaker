import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';

import Dashboard from './scenes/dashboard/Dashboard.jsx';

ReactDOM.render(
    <Dashboard></Dashboard>,
    document.getElementById('root')
);
