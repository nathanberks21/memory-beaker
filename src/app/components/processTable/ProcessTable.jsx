import React from 'react';

import { Table } from 'react-bootstrap';

export default class ProcessTable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Table striped bordered condensed hover>
                <thead>
                    <tr>
                        <th>PID</th>
                        <th>Process</th>
                        <th>Starting Memory</th>
                        <th>Current Memory</th>
                        <th>Memory/s</th>
                        <th>Capture Time</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {
                    Object.values(this.props.processList).map(process => {
                        const button = this.props.selectedPids.has(process.pid) ?
                            (
                                <button type="button" className="btn btn-danger btn-lg"
                                    onClick={this.props.selectProcess.bind(this, process.pid)}>
                                    <span className="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                </button>
                            ) : (
                                <button type="button" className="btn btn-success btn-lg"
                                    onClick={this.props.selectProcess.bind(this, process.pid)}>
                                    <span className="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                </button>
                            );
                        return (
                            <tr key={process.pid}>
                                <td>{process.pid}</td>
                                <td>{process.name}</td>
                                <td>{bytesConversion(process.startMemory)}</td>
                                <td>{bytesConversion(process.memory)}</td>
                                <td>{getAverageMemory(process.memory, process.startMemory, this.props.secondsPassed)}</td>
                                <td>{secondsToTime(this.props.secondsPassed)}</td>
                                <td>{button}</td>
                            </tr>
                        );
                    })
                }
                </tbody>
            </Table>
        );
    }
}

function getAverageMemory(memory, startMemory, secondsPassed) {
    if (memory < startMemory) {
        const avergeIncrease = (startMemory - memory) / secondsPassed;
        return `-${bytesConversion(avergeIncrease)}`;
    }
    const avergeIncrease = (memory - startMemory) / secondsPassed;
    return bytesConversion(avergeIncrease);
}

function secondsToTime(seconds) {
    let days = Math.floor(seconds / (3600 * 24));
    seconds  -= days * 3600 * 24;
    days = days > 0 ? `${days}d ` : '';
    let hours   = Math.floor(seconds / 3600);
    seconds  -= hours * 3600;
    hours = hours > 0 ? `${hours}h ` : '';
    let mins = Math.floor(seconds / 60);
    seconds  -= mins * 60;
    mins = mins > 0 ? `${mins}m ` : '';
    seconds = seconds > 0 ? `${seconds}s` : '';
    return `${days}${hours}${mins}${seconds}`;
}

const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
function bytesConversion(x) {
  let l = 0, n = parseInt(x, 10) || 0;
  while(n >= 1024 && ++l) {
    n = n/1024;
  }
  return(n.toFixed(n >= 10 || l < 1 ? 0 : 1) + ' ' + units[l]);
}
