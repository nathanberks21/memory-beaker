import { ReplaySubject } from 'rxjs';
import { ipcRenderer } from 'electron';

const GET_ALL_PROCESSES_INTERVAL = 1000;
const processes$ = new ReplaySubject(1);
let secondsPassed = 0, processList = {};

ipcRenderer.on('get-all-processes-reply', updateProcessList);

setInterval(() => ipcRenderer.send('get-all-processes'), GET_ALL_PROCESSES_INTERVAL);

function updateProcessList(ev, currentProcesses) {
    secondsPassed++;
    if (!Object.keys(processList).length) {
        processList = Object.keys(currentProcesses).reduce((acc, key) => {
            acc[key] = newValue(currentProcesses[key]);
            return acc;
        }, {});
        return processes$.next({ processList, secondsPassed });
    }
    processList = updateAndRemoveMemoryValues(currentProcesses);
    processList = addNewValues(currentProcesses);
    processes$.next({ processList, secondsPassed });
}

function newValue(process) {
    process.memoryMin = process.memory;
    process.memoryMax = process.memory;
    process.startMemory = process.memory;
    return process;
}

function updateAndRemoveMemoryValues(currentProcesses) {
    return Object.keys(processList).reduce((acc, key) => {
        if (key in currentProcesses) {
            acc[key] = processList[key];
            acc[key].memory = currentProcesses[key].memory;
            acc[key].memoryMin = currentProcesses[key].memory < processList[key].memoryMin
                ? currentProcesses[key].memory
                : processList[key].memoryMin;
            acc[key].memoryMax = currentProcesses[key].memory > processList[key].memoryMax
                ? currentProcesses[key].memory
                : processList[key].memoryMax;
        }
        return acc;
    }, {});
}

function addNewValues(currentProcesses) {
    return Object.keys(currentProcesses).reduce((allProcessList, key) => {
        if (!key in allProcessList) {
            allProcessList[key] = newValue(currentProcesses[key]);
        }
        return allProcessList;
    }, processList);
}

export {
    processes$
};
