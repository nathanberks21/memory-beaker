const path = require('path');
const { app, BrowserWindow } = require('electron');

require('./ipcHandler');

const urlToLoad = isPackaged() ? `file://${__dirname}/dist/app/index.html` : 'http://localhost:8080/';

let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({ width: 800, height: 600 });
  mainWindow.on('closed', () => mainWindow = null);
  mainWindow.loadURL(urlToLoad);
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow();
  }
});

function isPackaged() {
  const execFile = path.basename(process.execPath).toLowerCase();
  if (process.platform === 'win32') {
    return execFile !== 'electron.exe'
  }
  return execFile !== 'electron'
}
