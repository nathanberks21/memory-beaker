const psList = require('ps-list');
const pidUsage = require('pidusage');

function getProcessNames(processes) {
    return processes
        .reduce((processList, process) => {
            processList[process.pid] = {
                name: process.name,
                pid: process.pid
            };
            return processList;
        }, {});
}

function getUsageByPid(processes) {
    const pids = Object.keys(processes);
    return Promise.all([pidUsage(pids), processes])
}

function mergePidInfo([pidUsage, pidNames]) {
    return Object.keys(pidNames).reduce((obj, key) => {
        if (pidUsage[key] && pidNames[key]) {
            obj[key] = Object.assign(pidUsage[key], pidNames[key]);
        }
        return obj;
    }, {});
}

function getAll() {
    return psList()
        .then(getProcessNames)
        .then(getUsageByPid)
        .then(mergePidInfo);
}

function getProcessUsageById(processId) {
    return pidUsage(processId);
}

module.exports = {
    getAll,
    getProcessUsageById
}
